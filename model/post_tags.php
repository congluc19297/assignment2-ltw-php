<?php

class PostTag {
    // Private
    private $conn;
    private $table_name = "post_tags";

    // Public
    public $tagid;
    public $postid;
    public $tag_index;
    public $tag_value;

    public function __construct($db){
        $this->conn = $db;
    }
    
}

?>