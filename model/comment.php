<?php
class Comment {
    // Private
    private $conn;
    private $table_name = "comments";
    private $table_cate = "post_tags";
    private $table_member = "members";
    private $table_post = "posts";

    // Public
    public $commentID;
    public $userID;
    public $postID;
    public $comment;
    public $timeAdded;

    public function __construct($db){
        $this->conn = $db;
    }

    public function createNewComment($data, $userid) {
        $table = $this->table_name;
        $postid = $data->postid;
        $comment = htmlspecialchars($data->comment, ENT_QUOTES);
        // htmlspecialchars_decode()
        $query = "INSERT INTO `$table` (`USERID`, `PID`, `comment`) VALUES ($userid, '$postid', '$comment')";
        $result = $this->conn->query($query);
        // var_dump(htmlspecialchLiars($comment, ENT_QUOTES));
        if($result) {
            $last_id = mysqli_insert_id($this->conn);
            return $this->getCommentByID($last_id);
        }
        
        return null;
    }

    public function getCommentByID($cid) {
        $table = $this->table_name;
        $query = "SELECT * from `$table` WHERE `CID`=$cid";
        $result = $this->conn->query($query);
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    return $row;
                }
            }
        }
        return null;
    }

    public function getCommentByPostID($postid) {
        /*
            SELECT `comments`.`CID`, `comments`.`PID`, `members`.`USERID`,`members`.`fullname`, `members`.`profilepicture`, `comments`.`comment`,`comments`.`time_added`
            FROM `comments`
            INNER JOIN `members`
            ON `comments`.`PID`=28 AND `comments`.`USERID`=`members`.`USERID`
            ORDER BY `comments`.`time_added` DESC
        */
        $tcom = $this->table_name; 
        $tmem = $this->table_member;

        $query = array();
        $query[] = "SELECT `$tcom`.`CID`, `$tcom`.`PID`, `$tmem`.`USERID`,`$tmem`.`fullname`, `$tmem`.`profilepicture`, `$tcom`.`comment`,`$tcom`.`time_added`";
        $query[] = "FROM `$tcom`";
        $query[] = "INNER JOIN `$tmem`";
        $query[] = "ON `$tcom`.`PID`=$postid AND `$tcom`.`USERID`=`$tmem`.`USERID`";
        $query[] = "ORDER BY `comments`.`time_added` ASC";
        $query = implode(" ", $query);

        $result = $this->conn->query($query);
        // var_dump($query);
        $arrResult = array();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    array_push($arrResult, $row);
                }
            }
            return $arrResult;
        }   
        return $arrResult;
    }
}
?>