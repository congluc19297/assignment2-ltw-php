<?php
// require_once '../api/config.php';
// include_once '../helpers/jwt-helpers.php';


class Member {
    // private
    private $conn;
    private $table_name = "members";

    // public
    public $userid;
    public $email;
    // public $username;
    public $password;
    public $fullname;
    public $gender;
    public $description;
    public $yourViewed;
    public $profileViews;
    public $youViewed;
    public $lastlogin;
    public $status;
    public $profilepicture;
    public $permission;

    public function __construct($db){
        $this->conn = $db;
    }

    public function register($data) {
        $email = $data->email;
        $fullname = $data->fullname;
        $password = md5($data->password);
        $table = $this->table_name;

        $_sql = "SELECT * FROM `$table` WHERE `email`='$email'";

        if($this->conn->query($_sql)->num_rows > 0) {
            return -1;
        } else {
            $query = "INSERT INTO `$table` (`email`, `fullname`, `password`) VALUES ('$email', '$fullname', '$password')";
            $result = $this->conn->query($query);

            if($result) {
                
                $_result = $this->conn->query($_sql);

                if($_result->num_rows > 0) {
                    while($row = $_result->fetch_assoc()) {
                        return $row;
                    }
                } else {
                    return $_sql;
                }
            }
            return -2;
        }
    }

    public function getUserById($USERID) {
        $table = $this->table_name;
        $query = "SELECT * FROM `$table` WHERE `USERID`=$USERID";
        $result = $this->conn->query($query);
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $row['password'] = ''; //For Security
                if($row['permission'] == '_member') $row['permission'] = 'member';
                else $row['permission'] = 'admin';
                return $row;
            }
        }
        return false;
    }

    public function getUserByEmailPassword($email, $password) {
        $table = $this->table_name;
        $password = md5($password);
        $query = "SELECT * FROM `$table` WHERE `email`='$email' AND `password`='$password'";

        $result = $this->conn->query($query);
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                if($row['permission'] == '_member') $row['permission'] = 'member';
                else $row['permission'] = 'admin';
                return $row;
            }
        }
        return false;
    }

    public function updateInfoMember($data, $USERID) {
        $table = $this->table_name;
        $fullname = $data['fullname'];
        $gender = $data['gender'];
        $description = $data['description'];
        $profilepicture = $data['profilepicture'];

        $user = $this->getUserById($USERID);
        if($user) {
            $query = "UPDATE `$table` SET `fullname` = '$fullname', `gender` = '$gender', `description` = '$description', `profilepicture` = '$profilepicture' WHERE `members`.`USERID` = $USERID";

            $result = $this->conn->query($query);
            if($result) {
                $user['password'] = ''; //For Security
                $user['fullname'] = $fullname;
                $user['gender'] = $gender;
                $user['description'] = $description;
                $user['profilepicture'] = $profilepicture;
                return $user;
            }
        }
        return false;
    }

    public function changePassword($user, $oldPass, $newPass) {
        $table = $this->table_name;
        $checkUser = $this->getUserByEmailPassword($user['email'], $oldPass);
        if($checkUser && $checkUser['USERID'] == $user['USERID']) {
            $userid = $user['USERID'];
            $newPass = md5($newPass);
            $query = "UPDATE `$table` SET `password` = '$newPass' WHERE `members`.`USERID` = $userid";
            $result = $this->conn->query($query);
            if($result) {
                $result['password'] = '';
                return $result;
            }
            return 0;
        }
        return -1;
    }

    public function getCountUser() {
        $query = "SELECT COUNT(`members`.`USERID`) AS `count` FROM `members`";
        $result = $this->conn->query($query);
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    return $row['count'];
                }
            }
            return 0;
        }
        return null;
    }

    public function getUsersPaging($pagesize, $currpage) {
        $offset = ($currpage - 1) * $pagesize;
        // $query = "SELECT * FROM `members` LIMIT $pagesize OFFSET $offset";
        $query = array();
        $query[] = "SELECT * FROM `members`";
        $query[] = "ORDER BY `members`.`USERID` DESC";
        $query[] = "LIMIT $pagesize OFFSET $offset";
        $query = implode(" ", $query);

        $result = $this->conn->query($query);
        $arrData = array();

        $count = $this->getCountUser();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $row['password'] = ''; //For security;
                    if($row['permission'] == '_member') $row['permission'] = 'member';
                    else $row['permission'] = 'admin';
                    array_push($arrData, $row);
                }
            }
        }
        return array(
            'users' => $arrData,
            'total' => $count
        );
    }

    public function toggleStatusUserByPID($userid, $user) {
        // UPDATE `posts` SET `status` = '0' WHERE `posts`.`PID` = 28;
        $table = $this->table_name;
        $oldStatus = $user['status'];
        if($oldStatus === '0') { $newStatus = '1'; }
        else { $newStatus = '0'; }
        $query = "UPDATE `$table` SET `status` = '$newStatus' WHERE `$table`.`USERID` = $userid";
        return $this->conn->query($query);
    }
}