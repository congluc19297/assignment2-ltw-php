<?php
error_reporting(E_ERROR | E_PARSE);

class Database {
    private $host = "localhost";
    private $db_name = "assignment2";
    private $username = "root";
    private $password = "";
    public $conn;

    public function __construct() {
        $this->conn = null;
        $connect = mysqli_connect($this->host,$this->username,$this->password,$this->db_name);
        if(!$connect) {
            die("<h3>Database connect error! ". mysqli_connect_error() ."</h3>");
        } else {
            $connect->query("SET NAMES 'utf8'");
			$connect->query("SET CHARACTER SET 'utf8'");
            $this->conn = $connect;
        }
    }

}