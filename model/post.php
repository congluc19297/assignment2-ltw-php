<?php

class Post {
    // Private
    private $conn;
    private $table_name = "posts";
    private $table_cate = "post_tags";
    private $table_member = "members";

    // Public
    public $postid;
    public $userid;
    public $url_image;
    public $post_content;
    public $status;
    public $time_added;

    public function __construct($db){
        $this->conn = $db;
    }
// UPDATE `posts` SET `url_image` = '', `post_content` = '' WHERE `posts`.`PID` = 25;

    public function createNewPost($data, $userid) {
        date_default_timezone_set("Asia/Bangkok");
        $table = $this->table_name;
        $url_image = $data['url_image'];
        $post_content = htmlspecialchars($data['post_content'], ENT_QUOTES);

        // $time_added = date('m/d/Y H:i:s');
        try {
            $this->conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            $query = "INSERT INTO `$table` (`USERID`, `url_image`, `post_content`) VALUES ($userid, '$url_image', '$post_content')";
            $result = $this->conn->query($query);
            // echo $query;
            if($result) {
                $last_id = mysqli_insert_id($this->conn);
                $postNew = $this->getPostByPID($last_id);
                if($postNew != null) {
                    $arrCategory = explode(',', $data['category']);
    
                    $categories = $this->addCategoryPost($last_id, $arrCategory);
                    $this->conn->commit();
                    return array(
                        'post' => $postNew,
                        'categories' => $categories 
                    );
                }
                return -1; 
            }
            return null;
        } catch (\Throwable $th) {
            $this->conn->rollback();
            return null;
        }
    }
    public function deleteAllCategoriesByPostID($postid) {
        $query = "DELETE FROM `post_tags` WHERE `post_tags`.`PID` = $postid";
        return $this->conn->query($query);
    }
    public function editPost($data, $postid) {
        date_default_timezone_set("Asia/Bangkok");
        $table = $this->table_name;
        $url_image = $data['url_image'];
        $post_content = htmlspecialchars($data['post_content'], ENT_QUOTES);

        // $time_added = date('m/d/Y H:i:s');
        try {
            $this->conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            $this->deleteAllCategoriesByPostID($postid);
            // $query = "INSERT INTO `$table` (`USERID`, `url_image`, `post_content`) VALUES ($userid, '$url_image', '$post_content')";
            $query = "UPDATE `posts` SET `url_image` = '$url_image', `post_content` = '$post_content' WHERE `posts`.`PID` = $postid";
            $result = $this->conn->query($query);
            // echo $query;
            if($result) {
                // $last_id = mysqli_insert_id($this->conn);
                $postNew = $this->getPostByPID($postid);
                $arrCategory = explode(',', $data['category']);

                $categories = $this->addCategoryPost($postid, $arrCategory);
                $this->conn->commit();
                return array(
                    'post' => $postNew,
                    'categories' => $categories 
                );
            }
            return null;
        } catch (\Throwable $th) {
            $this->conn->rollback();
            return null;
        }
    }

    public function addCategoryPost($pid, $categories) {
        $lengh = count($categories);
        $table = $this->table_cate;
        for ($i=0; $i < $lengh; $i++) {
            $tag_index = $categories[$i];
            $tag_value = $this->getCategoryById($tag_index);
            $query = "INSERT INTO `$table` (`PID`, `tag_index`, `tag_value`) VALUES ($pid, '$tag_index', '$tag_value')";
            $result = $this->conn->query($query);
        }
        return $this->getAllCategoryByPostID($pid);
    }

    public function getAllCategoryByPostID($pid) {
        $table = $this->table_cate;
        $query = "SELECT * FROM `$table` WHERE `PID`=$pid";
        $result = $this->conn->query($query);
        $arrResult = array();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    array_push( $arrResult, $row );
                }
            }
        }
        return $arrResult;
    }

    public function getPostByPID($pid) {
        $table = $this->table_name;
        $query = "SELECT * FROM `$table` WHERE `PID`=$pid";
        $result = $this->conn->query($query);
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    return $row;
                }
            }
        }
        return null;
    }

    public function getSingLePostByPID($pid) {
        $result = $this->getPostByPID($pid);
        if($result != null) {
            $cate = $this->getAllCategoryByPostID($pid);
            return array(
                'post' => $result,
                'categories' => $cate 
            );
        }   
        return null;
    }

    public function getListPostByUserID($userid) {
        /*
        Example:
            SELECT `members`.`USERID`,`members`.`fullname`, `members`.`profilepicture`, `posts`.`url_image`, `posts`.`PID`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status` 
            FROM `posts` 
            INNER JOIN `members` 
            ON `members`.`USERID` = 3 AND `members`.`USERID` = `posts`.`USERID` 
            ORDER BY `posts`.`time_added` DESC
        */
        $table = $this->table_name;
        $table_member = $this->table_member;
        // $query[] = "SELECT `$table_member`.`USERID`,`$table_member`.`fullname`, `$table_member`.`profilepicture`, `$table`.`url_image`, `$table`.`PID`, `$table`.`post_content`, `$table`.`time_added`, `$table`.`status`";
        // $query[] = "FROM `$table`";
        // $query[] = "INNER JOIN `$table_member`";
        // $query[] = "ON `$table_member`.`USERID` = $userid AND `$table_member`.`USERID` = `$table`.`USERID`";
        // $query[] = "ORDER BY `$table`.`time_added` ASC";
        $query[] = "SELECT `UserPost`.*, `NumberComment`.`count`";
        $query[] = "FROM (";
        $query[] = "    SELECT `members`.`USERID`,`members`.`fullname`, `members`.`profilepicture`, `posts`.`url_image`, `posts`.`PID`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status` ";
        $query[] = "    FROM `posts` ";
        $query[] = "    INNER JOIN `members` ";
        $query[] = "    ON `members`.`USERID` = $userid AND `members`.`USERID` = `posts`.`USERID` ";
        $query[] = "    ORDER BY `posts`.`time_added` DESC";
        $query[] = ") AS `UserPost`";
        $query[] = "LEFT JOIN (";
        $query[] = "    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` ";
        $query[] = "    FROM `comments`";
        $query[] = "    GROUP BY `comments`.`PID`";
        $query[] = ") AS `NumberComment`";
        $query[] = "ON `UserPost`.`PID`=`NumberComment`.`PID`";
        $query = implode(" ", $query);
        // print_r($query);
        $result = $this->conn->query($query);
        $arrResult = array();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    array_push($arrResult, $row);
                }
            }
            return $arrResult;
        } else {
            return null;
        }
    }

    public function deletePostByPID($postid, $post, $categories) {
        /*
            DELETE FROM `post_tags` WHERE `post_tags`.`TAG_ID` = 30;
            DELETE FROM `post_tags` WHERE `post_tags`.`TAG_ID` = 31;
            DELETE FROM `post_tags` WHERE `post_tags`.`TAG_ID` = 32;
            DELETE FROM `posts` WHERE `posts`.`PID` = 29;
        */
        // $this->conn->query("START TRANSACTION");
        try {
            $this->conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            $this->deleteCategories($categories);
            $table = $this->table_name;

            $query = "DELETE FROM `$table` WHERE `$table`.`PID` = $postid";
            $result = $this->conn->query($query);
            if($result) {
                $this->conn->commit();
                return true;
            }
            $this->conn->rollback();
            return false;
        } catch (\Throwable $th) {
            $this->conn->rollback();
            return false;
        }   
    }

    public function deleteCategories($categories) {
        $table_cate = $this->table_cate;
        $len = count($categories);
        for ($idx=0; $idx < $len; $idx++) {
            $TAGID = $categories[$idx]['TAG_ID'];
            $query = "DELETE FROM `$table_cate` WHERE `$table_cate`.`TAG_ID` = $TAGID";
            $this->conn->query($query);
        }
    }

    public function toggleStatusPostByPID($postid, $post) {
        // UPDATE `posts` SET `status` = '0' WHERE `posts`.`PID` = 28;
        $table = $this->table_name;
        $oldStatus = $post['status'];
        if($oldStatus === '0') { $newStatus = '1'; }
        else { $newStatus = '0'; }
        $query = "UPDATE `$table` SET `status` = '$newStatus' WHERE `$table`.`PID` = $postid";
        return $this->conn->query($query);
    }

    public function searchPost($searchQuery) {
        //SELECT * FROM `posts` WHERE `posts`.`post_content` like '%thù%'
        /*
        Only search content post
            SELECT `members`.`profilepicture`, `members`.`fullname`, `cp`.`PID`, `cp`.`url_image`, `cp`.`post_content`, `cp`.`time_added`, `cp`.`status`
            FROM (
                SELECT * FROM `posts` WHERE `posts`.`post_content` like '%th%'
            ) AS `cp`
            LEFT JOIN `members`
            ON `members`.`USERID` = `cp`.`USERID`
            ORDER BY `cp`.`time_added` DESC
        Search content post and fullname
        SELECT * 
        FROM (
            SELECT `members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`
            FROM `posts`
            LEFT JOIN `members`
            ON `members`.`USERID` = `posts`.`USERID`
        ) AS `search_table`
        WHERE `search_table`.`post_content` like '%Trần Công Lực%' OR `search_table`.`fullname` like '%Trần Công Lực%'
        ORDER BY `search_table`.`time_added` DESC
        */
        $table = $this->table_name;
        $query = [];
        // $query[] = "SELECT *";
        // $query[] = "FROM (";
        // $query[] = "    SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`";
        // $query[] = "    FROM `posts`";
        // $query[] = "    LEFT JOIN `members`";
        // $query[] = "    ON `members`.`USERID` = `posts`.`USERID`";
        // $query[] = ") AS `search_table`";
        // $query[] = "WHERE `search_table`.`post_content` like '%$searchQuery%' OR `search_table`.`fullname` like '%$searchQuery%'";
        // $query[] = "ORDER BY `search_table`.`time_added` DESC";
        $query[] = "SELECT `UserPost`.*, `NumberComment`.`count`";
        $query[] = "FROM (";
        $query[] = "    SELECT * ";
        $query[] = "    FROM (";
        $query[] = "        SELECT `members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`";
        $query[] = "        FROM `posts`";
        $query[] = "        LEFT JOIN `members`";
        $query[] = "        ON `members`.`USERID` = `posts`.`USERID`";
        $query[] = "    ) AS `search_table`";
        $query[] = "    WHERE `search_table`.`post_content` like '%$searchQuery%' OR `search_table`.`fullname` like '%$searchQuery%'";
        $query[] = ") AS `UserPost`";
        $query[] = "LEFT JOIN (";
        $query[] = "    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` ";
        $query[] = "    FROM `comments`";
        $query[] = "    GROUP BY `comments`.`PID`";
        $query[] = ") AS `NumberComment`";
        $query[] = "ON `UserPost`.`PID`=`NumberComment`.`PID`";
        $query = implode(" ", $query);

        $result = $this->conn->query($query);
        
        $resultArr = array();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    array_push($resultArr, $row);
                }
            }
        }
        return $resultArr;
    }

    public function getCountPost() {
        $query = "SELECT COUNT(`posts`.`PID`) AS `count` FROM `posts`";
        $result = $this->conn->query($query);
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    return $row['count'];
                }
            }
            return 0;
        }
        return null;
    }

    public function getListPostPaging($pagesize, $currPage) {
        /**
         * pagesize = 3
         * 0 -> 3 -> 6
         */
        $offset = ($currPage - 1) * $pagesize;
        $query = [];
        $query[] = "SELECT `UserPost`.*, `NumberComment`.`count`";
        $query[] = "FROM (";
        $query[] = "    SELECT * ";
        $query[] = "    FROM (";
        $query[] = "        SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`";
        $query[] = "        FROM `posts`";
        $query[] = "        LEFT JOIN `members`";
        $query[] = "        ON `members`.`USERID` = `posts`.`USERID`";
        $query[] = "        ORDER BY `posts`.`time_added` DESC";
        $query[] = "        LIMIT $pagesize OFFSET $offset";
        $query[] = "    ) AS `search_table`";
        $query[] = ") AS `UserPost`";
        $query[] = "LEFT JOIN (";
        $query[] = "    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` ";
        $query[] = "    FROM `comments`";
        $query[] = "    GROUP BY `comments`.`PID`";
        $query[] = ") AS `NumberComment`";
        $query[] = "ON `UserPost`.`PID`=`NumberComment`.`PID`";
        $query = implode(" ", $query);

        $result = $this->conn->query($query);
        // var_dump($result);
        $resultArr = array();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    array_push($resultArr, $row);
                }
            }
        }
        return $resultArr;
    }

    public function getListPostPagingByCategoriesIndex($pagesize, $currPage, $tagIndex) {
        $offset = ($currPage - 1) * $pagesize;
        $query = [];
        $query[] = "SELECT `UserPost`.*, `NumberComment`.`count`";
        $query[] = "FROM (";
        $query[] = "    SELECT * ";
        $query[] = "    FROM (";
        $query[] = "        SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `_posts`.`PID`, `_posts`.`url_image`, `_posts`.`post_content`, `_posts`.`time_added`, `_posts`.`status`, `_posts`.`tag_index`, `_posts`.`tag_value`";
        $query[] = "        FROM (";
        $query[] = "            SELECT `posts`.*, `POST_TAG_PID`.`tag_index`, `POST_TAG_PID`.`tag_value`";
        $query[] = "            FROM (";
        $query[] = "                SELECT `post_tags`.`PID`, `post_tags`.`tag_index`, `post_tags`.`tag_value`";
        $query[] = "                FROM `post_tags` WHERE `post_tags`.`tag_index`=$tagIndex";
        $query[] = "            ) `POST_TAG_PID`";
        $query[] = "            LEFT JOIN `posts`";
        $query[] = "            ON `posts`.`PID`=`POST_TAG_PID`.`PID`";
        $query[] = "            ORDER BY `posts`.`time_added` DESC";
        $query[] = "            LIMIT $pagesize OFFSET $offset";
        $query[] = "        ) AS `_posts`";
        $query[] = "        LEFT JOIN `members`";
        $query[] = "        ON `members`.`USERID` = `_posts`.`USERID`";
        $query[] = "    ) AS `search_table`";
        $query[] = ") AS `UserPost`";
        $query[] = "LEFT JOIN (";
        $query[] = "    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` ";
        $query[] = "    FROM `comments`";
        $query[] = "    GROUP BY `comments`.`PID`";
        $query[] = ") AS `NumberComment`";
        $query[] = "ON `UserPost`.`PID`=`NumberComment`.`PID`";
        $query = implode(" ", $query);

        $result = $this->conn->query($query);
        $resultArr = array();
        if($result) {
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    array_push($resultArr, $row);
                }
            }
        }
        return $resultArr;
    }

    public function getCategoryById($key) {
        $categories = Array(
            '',
            'Ảnh troll',
            'Cách gặp ma',
            'Clip funvl',
            'Xác ướp',
            'Ảo tưởng sức mạnh',
            'Running man',
            'Ảnh bựa VL',
            'Faptv',
            '500 anh em',
            'Ancient aliens',
            'Video cảm động',
            'Siêu nhân',
            'Video kinh dị',
            'Thánh cuồng',
            'Comment hài',
            'Nhạc Remix hay',
            'Ảnh ấn tượng',
            'Pháo hoa Tết',
            'Ji Suk Jin',
            'Cao Bá Hưng',
            'Hài Quang Thắng',
            'Xăm trổ',
            'Chuyện tình lãng mạn',
            'Giang hồ'
        );
        if($key >= 0 && $key < count($categories)) {
            return $categories[$key];
        }
        return null;
    }
}

?>