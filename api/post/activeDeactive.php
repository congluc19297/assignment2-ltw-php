<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, PUT, DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';
include_once '../../model/post.php';

$database = new Database();

$post = new Post($database->conn);
$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = json_decode(file_get_contents("php://input"));

if($method == 'POST') {
    $PID = $data->postid;
    $token = substr(getallheaders()['Authorization'], 7);
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);
        $user = $member->getUserById($dataDecode->id);
        if(!$user) {
            http_response_code(401);
            echo json_encode(
                array( 
                    "status" => 500,
                    "error" => 'Unauthorized',
                    "token" => $token
                )
            );  
            return;
        }
    } catch( \Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 500,
                "error" => 'Unauthorized',
                "token" => $token
            )
        );
        return;
    }
    // Verify token success
    
    $postInfo = $post->getSingLePostByPID($PID);
    if($postInfo) {
        if($postInfo['post']['USERID'] == $user['USERID']) {
            
            $result = $post->toggleStatusPostByPID($PID, $postInfo['post']);
            if($result) {
                http_response_code(200);
                echo json_encode(
                    array( 
                        "status" => 200,
                        "message" => "Thay đổi chế độ xem của bài viết thành công!"
                    )
                );
                return;
            }
            http_response_code(500);
            echo json_encode(
                array( 
                    "status" => 500,
                    "error" => "Có lỗi xảy ra. Thay đổi chế độ xem của bài viết không thành công!",
                    "message" => getStatusCodeMessage(500)
                )
            );
            return;
        } else {
            http_response_code(403);
            echo json_encode(
                array( 
                    "status" => 403,
                    "error" => "Bạn không được phép truy cập vào bài viết này!",
                    "message" => getStatusCodeMessage(403)
                )
            );  
        }
        return;
    }
       
    http_response_code(400);
    echo json_encode(
        array( 
            "status" => 400,
            "error" => "Bài viết không tồn tại!",
            "message" => getStatusCodeMessage(400)
        )
    );
    return;
} else {
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 405,
            "error" => "Access denied!",
            "message" => getStatusCodeMessage(405)
        )
    );
    return;
}
?>