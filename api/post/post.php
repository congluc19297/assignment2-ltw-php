<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, PUT, DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';
include_once '../../model/post.php';

$database = new Database();

$member = new Member($database->conn);
$post = new Post($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

// $data = json_decode(file_get_contents("php://input")); -> POST
$dataGET = $_REQUEST; //-> GET
// $dataPUT = json_decode(file_get_contents("php://input"));

if($method == 'GET' && !empty($dataGET['postid'])) {
    // $token = substr(getallheaders()['Authorization'], 7);
    // try {
    //     $dataDecode = JWT::decode($token, SERET_SERVER_KEY);

    //     // Verify token success
    //     $POSTID = $dataGET['postid'];
    //     $result = $post->getSingLePostByPID($POSTID);
    //     if($result) {
    //         http_response_code(200);
    //         echo json_encode(
    //             array( 
    //                 "status" => 200,
    //                 "message" => "Success!",
    //                 "data" => $result
    //             )
    //         );
    //         return;
    //     } 
    //     http_response_code(500);
    //     echo json_encode(
    //         array( 
    //             "status" => 500,
    //             "error" => "Không tìm thấy bài viết trong cơ sở dữ liệu!"
    //         )
    //     );
    //     return;
    // } catch( \Throwable $th) {
    //     http_response_code(401);
    //     echo json_encode(
    //         array( 
    //             "status" => 500,
    //             "error" => 'Unauthorized',
    //             "token" => $token
    //         )
    //     );
    //     return;
    // }
    $POSTID = $dataGET['postid'];
    $result = $post->getSingLePostByPID($POSTID);
    if($result) {
        http_response_code(200);
        echo json_encode(
            array( 
                "status" => 200,
                "message" => "Success!",
                "data" => $result
            )
        );
        return;
    } 
    http_response_code(500);
    echo json_encode(
        array( 
            "status" => 500,
            "error" => "Không tìm thấy bài viết trong cơ sở dữ liệu!"
        )
    );
    return;
}
http_response_code(200);
echo json_encode(
    array( 
        "status" => 502,
        "error" => "Access denied!",
        "message" => getStatusCodeMessage(502)
    )
);