<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';

include_once '../../model/database.php';
include_once '../../model/post.php';

$database = new Database();

$post = new Post($database->conn);

$method = $_SERVER['REQUEST_METHOD'];
$data = $_GET;

if($method == 'GET' &&
    isset($data['pagesize']) &&
    isset($data['currPage']) &&
    isset($data['tagIndex'])
) {
    // $result = $post->searchPost($searchtxt);
    // $count = $post->getCountPost();
    $result = $post->getListPostPagingByCategoriesIndex($data['pagesize'], $data['currPage'], $data['tagIndex']);

    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 200,
            "message" => "Thanh cong",
            "posts" => $result,
        )
    );  

} else {
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 405,
            "error" => "Access denied!",
            "message" => getStatusCodeMessage(405),
            "data" => $data
        )
    );
}
?>