<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';
include_once '../../model/post.php';

$database = new Database();

$member = new Member($database->conn);
$post = new Post($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$dataGET = $_REQUEST; //-> GET

if($method == 'GET' && !empty($dataGET['userid'])) {
    $token = substr(getallheaders()['Authorization'], 7);
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);

        // Verify token success
        $USERID = $dataGET['userid'];
        $result = $post->getListPostByUserID($USERID);
        if(count($result) >= 0) {
            http_response_code(200);
            // $result['password'] = ''; //For Security
            echo json_encode(
                array( 
                    "status" => 200,
                    "message" => "Success!",
                    "posts" => $result
                )
            );
            return;
        } else {
            http_response_code(200);
            echo json_encode(
                array( 
                    "status" => 500,
                    "error" => "Có lỗi xảy ra. Vui lòng thử lại!",
                    "system" => error_get_last()
                )
            );
            return;
        }
    } catch( \Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 500,
                "error" => 'Unauthorized',
                "token" => $token
            )
        );
        return;
    }
}
http_response_code(200);
echo json_encode(
    array( 
        "status" => "500",
        "error" => "Access denied!",
        "system" => error_get_last()
    )
);