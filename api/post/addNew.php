<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, PUT, DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';
include_once '../../model/post.php';

$database = new Database();

$post = new Post($database->conn);
$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = $_REQUEST; 

if($method == "POST") {
    $token = substr(getallheaders()['Authorization'], 7);
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);
        $result = $member->getUserById($dataDecode->id);
        if(!$result) {
            http_response_code(400);
            echo json_encode(
                array( 
                    "status" => 400,
                    "error" => "Token invalid!"
                )
            );
            return;
        }
    } catch (\Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 401,
                "error" => 'Unauthorized',
                "token" => $token,
                "system" => error_get_last()
            )
        );
        return;
    }
    if(isset($_FILES['obj_image'])) {
        $errors= array(); //xử lý nếu có lổi xãy ra và thông báo đến người dùng
        $allowed_ext= array('jpg','jpeg','png','gif');

        $file_name = $_FILES['obj_image']['name'];
        $file_size = $_FILES['obj_image']['size'];
        $file_tmp = $_FILES['obj_image']['tmp_name'];
        $file_type = $_FILES['obj_image']['type'];
        $file_ext=strtolower(end(explode('.',$_FILES['obj_image']['name'])));
        if(in_array($file_ext,$allowed_ext)=== false){
            array_push($errors, "Ảnh không hợp lệ, phần mở rộng phải là png, jpg hoặc jpeg");
        }
        if($file_size > 5242880) {
            array_push($errors, "Kích thước ảnh tối đa là 5MB");
        }
        if(empty($errors)) {
            if (!file_exists(PUBLIC_PATH_POST . $dataDecode->id )) {
                mkdir(PUBLIC_PATH_POST . $dataDecode->id, 0777, true);
            }
            if(move_uploaded_file($file_tmp, PUBLIC_PATH_POST . $dataDecode->id . DS . $file_name)) {
                $profilepicture = ORIGIN_PUBLIC_PATH_POST . $dataDecode->id . DS . $file_name;
                $data['url_image'] = $profilepicture;
            } else {
                http_response_code(500);
                echo json_encode(
                    array( 
                        "message" => "Có lỗi xảy ra trong quá trình xử lí. Vui lòng thử lại",
                        "error" => error_get_last()
                    )
                );
                return;
            }

        } else {
            http_response_code(500);
            echo json_encode(
                array( 
                    "message" => "Có lỗi xảy ra trong quá trình xử lí. Vui lòng thử lại",
                    "error" => $errors
                )
            );
            return;
        }
    }

    if( !empty($data['url_image']) &&
        !empty($data['post_content']) &&
        !empty($data['category']) ) {
        
        $newPost = $post->createNewPost($data, $dataDecode->id);
        if($newPost == null) {
            http_response_code(500);
            echo json_encode(
                array( 
                    "status" => 500,
                    "error" => "Đăng bài viết không thành công!",
                    "message" => getStatusCodeMessage(500),
                    "system" => error_get_last()
                    // "message" => "Thành công",
                    // "fullData" => $data,
                    // "category" => $data['category'],
                    // "fullFile" => $_FILES['obj_image'],
                    // "file_name" => $_FILES['obj_image']['name']
                )
            );
            return;
        } else {
            http_response_code(200);
            echo json_encode(
                array( 
                    "status" => 200,
                    "message" => "Đăng bài viết thành công!",
                    "data" => $newPost
                )
            );
            return;
        }  
    }else {
        http_response_code(400);
        echo json_encode(
            array( 
                "status" => 400,
                "error" => "Du lieu khong hop le!"
            )
        );
    }
    return;
}

http_response_code(200);
echo json_encode(
    array( 
        "status" => 500,
        "error" => "Access denied!"
    )
);