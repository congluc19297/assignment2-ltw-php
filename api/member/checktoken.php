<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
include_once '../../helpers/jwt-helpers.php';

$method = $_SERVER['REQUEST_METHOD'];
$data = json_decode(file_get_contents("php://input"));

if($method == 'POST' && !empty($data->token)) {
    try {
        $dataDecode = JWT::decode($data->token, SERET_SERVER_KEY);
        
        http_response_code(200);
        echo json_encode(
            array( 
                "code" => 200,
                "message" => "Token hợp lệ!",
                "user" => $dataDecode
            )
        );
        // $dataDecode->email
        return;
    } catch( \Throwable $th) {
        http_response_code(200);
        echo json_encode(
            array( 
                "code" => 500,
                "error" => "Token khong hợp lệ!"
            )
        );
        return;
    }
}

http_response_code(500);
echo json_encode(
    array( 
        "code" => 500,
        "error" => "Có lỗi xảy ra. Vui lòng thử lại!"
    )
);