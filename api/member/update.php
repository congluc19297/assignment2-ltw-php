<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, PUT, DELETE, OPTIONS");
header("Access-Control-Max-Age: 86400");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';

$database = new Database();

$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = $_REQUEST;

if($method == 'POST') {
    $token = substr(getallheaders()['Authorization'], 7);
    
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);
        $result = $member->getUserById($dataDecode->id);
        if(!$result) {
            http_response_code(400);
            echo json_encode(
                array( 
                    "status" => 400,
                    "error" => "Token invalid!"
                )
            );
            return;
        }
        $profilepicture = $result['profilepicture'];

        if(isset($_FILES['avatar'])) {
            $errors= array(); //xử lý nếu có lổi xãy ra và thông báo đến người dùng

            $file_name = $_FILES['avatar']['name'];
            $file_size = $_FILES['avatar']['size'];
            $file_tmp = $_FILES['avatar']['tmp_name'];
            $file_type = $_FILES['avatar']['type'];
            $file_ext=strtolower(end(explode('.',$_FILES['avatar']['name'])));
            if(in_array($file_ext,$expensions)=== false){
                array_push($errors, "Ảnh không hợp lệ, phần mở rộng phải là png, jpg hoặc jpeg");
            }
            if($file_size > 2097152) {
                array_push($errors, "Kích thước ảnh tối đa là 2MB");
            }
            if(empty($errors)) {
                if (!file_exists(PUBLIC_PATH_USER . $dataDecode->id )) {
                    mkdir(PUBLIC_PATH_USER . $dataDecode->id, 0777, true);
                }
                if(move_uploaded_file($file_tmp, PUBLIC_PATH_USER . $dataDecode->id . DS . $file_name)) {
                    $profilepicture = ORIGIN_PUBLIC_PATH_USER . $dataDecode->id . DS . $file_name;
                } else {
                    http_response_code(500);
                    echo json_encode(
                        array( 
                            "message" => "Có lỗi xảy ra trong quá trình xử lí. Vui lòng thử lại",
                            "error" => error_get_last()
                        )
                    );
                    return;
                }

            } else {
                http_response_code(500);
                echo json_encode(
                    array( 
                        "message" => "Có lỗi xảy ra trong quá trình xử lí. Vui lòng thử lại",
                        "error" => $errors
                    )
                );
                return;
            }
        }

        if(!empty($data['fullname']) &&
            !empty($data['gender']) &&
            !empty($data['description'])) {
            $arrayUser = array(
                'fullname' => $data['fullname'],
                'gender' => $data['gender'],
                'description' => $data['description'],
                'profilepicture' => $profilepicture,
            );

            $result = $member->updateInfoMember($arrayUser, $dataDecode->id);
            if($result) {
                http_response_code(200);
                echo json_encode(
                    array( 
                        "status" => 200,
                        "user" => $result,
                        "urlImage" => $profilepicture
                    )
                );

            }
        } else {
            http_response_code(400);
            echo json_encode(
                array( 
                    "status" => 400,
                    "error" => "Du lieu khong hop le!",
                    "data" => $_POST,
                )
            );
        }

        return;
    } catch( \Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 401,
                "error" => 'Unauthorized',
                "token" => $token,
                "test" => $dataDecode
            )
        );
        return;
    }
    
} else {
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 500,
            "error" => "Access denied!",
        )
    );
}
