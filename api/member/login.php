<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, DELETE, PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
include_once '../../helpers/jwt-helpers.php';
include_once '../../model/database.php';
include_once '../../model/member.php';

$database = new Database();
$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = json_decode(file_get_contents("php://input"));

if($method == 'POST') {
    if(!empty($data->email) && !empty($data->password)) {
        $email = $data->email; $password = $data->password;
        $pattern = '#^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$#';
        $flag = preg_match($pattern, $email);
        if($flag && strlen($password) >= 6) {
            
            $result = $member->getUserByEmailPassword($email, $password);
            if($result['status'] == "0") {
                http_response_code(203);
                echo json_encode(
                    array( 
                        "status" => 203,
                        "error" => "Tài khoản của bạn đã bị vô hiệu hoá.",
                    )
                );
                return;
            }
            if($result) {
                http_response_code(200);
                $result['password'] = ''; //For Security

                $payload = new stdClass();
                $payload->id = $result['USERID'];
                $payload->email = $result['email'];

                echo json_encode(
                    array( 
                        "status" => 200,
                        "message" => "Success!",
                        "user" => $result,
                        "token" => JWT::encode($payload, SERET_SERVER_KEY)
                    )
                );
                return;
            }
            http_response_code(200);
            echo json_encode(
                array( 
                    "status" => 500,
                    "error" => "Tài khoản hoặc mật khẩu không hợp lệ.",
                )
            );
            return;
        }
    }
    http_response_code(200);

    $error = Array();
    if(!$flag) array_push($error, 'Email không hợp lệ.');
    if(strlen($password) < 6) array_push($error, 'Password quá ngắn.');
    echo json_encode(
        array( 
            "status" => 500,
            "error" => $error,
        )
    );
    return;
    
}

http_response_code(200);
echo json_encode(
    array( 
        "status" => 500,
        "error" => "Access denied!",
    )
);