<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';

include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';

$database = new Database();

$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = json_decode(file_get_contents("php://input"));

if($method == 'POST') {
    $token = substr(getallheaders()['Authorization'], 7);
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);
        $result = $member->getUserById($dataDecode->id);

        if( 
            !empty($data->oldPassword) &&
            !empty($data->newPassword) &&
            !empty($data->reNewPassword)
        ) {
            if($data->oldPassword == $data->newPassword || $data->newPassword != $data->reNewPassword) {
                http_response_code(400);
                echo json_encode(
                    array( 
                        "status" => 400,
                        "error" => "Mật khẩu xác nhận không khớp hoặc không được trùng với mật khẩu cũ",
                        "message" => getStatusCodeMessage(400)
                    )
                );
                return;
            } else {
                $result = $member->changePassword($result, $data->oldPassword, $data->newPassword);
                if($result === -1) {
                    http_response_code(401);
                    echo json_encode(
                        array( 
                            "status" => 401,
                            "error" => "Mật khẩu cũ không đúng. Xin mời nhập lại",
                            "message" => getStatusCodeMessage(401)
                        )
                    ); 
                } else if($result === 0){
                    http_response_code(500);
                    echo json_encode(
                        array( 
                            "status" => 500,
                            "error" => "Có lỗi xảy ra khi thao tác với cơ sở dữ liệu. Vui lòng thử lại!",
                            "message" => getStatusCodeMessage(500)
                        )
                    ); 
                } else {
                    http_response_code(200);
                    echo json_encode(
                        array( 
                            "status" => 200,
                            "message" => "Thay đổi mật khẩu thành công"
                        )
                    ); 
                }
                return;
            }
    
        } else {
            http_response_code(400);
            echo json_encode(
                array( 
                    "status" => 400,
                    "error" => "Vui lòng nhập đầy đủ thông tin",
                    "message" => getStatusCodeMessage(400)
                )
            );   
        }
    } catch (\Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 401,
                "error" => "Token không hợp lệ",
                "message" => getStatusCodeMessage(401)
            )
        );
        return;
    }
    return;
} else {
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 200,
            "error" => "Có lỗi xảy ra. Vui lòng thử lại",
            "message" => getStatusCodeMessage(405)
        )
    );
}