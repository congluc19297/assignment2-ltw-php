<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, DELETE, PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
include_once '../../helpers/jwt-helpers.php';
include_once '../../model/database.php';
include_once '../../model/member.php';

$database = new Database();
$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = json_decode(file_get_contents("php://input"));

if($method == 'POST' && 
    !empty($data->email) && 
    !empty($data->fullname) && 
    !empty($data->password) && 
    !empty($data->repassword)) {
    
    if($data->password != $data->repassword) {
        http_response_code(200);
        echo json_encode(
            array( 
                "code" => 500,
                "error" => "Mật khẩu nhập lại không đúng!"
            )
        );
        return;
    }

    $response = $member->register($data);

    if($response == -1) {
        http_response_code(200);
    
        echo json_encode(
            array( 
                "code" => 500,
                "error" => "Tài khoản đã tồn tại!"
            )
        );
        return;
    } else if($response == -2) {
        http_response_code(200);
    
        echo json_encode(
            array( 
                "code" => 500,
                "error" => "Có lỗi trong quá trình thao tác với dữ liệu!"
            )
        );
        return;
    } else {
        $payload = new stdClass();
        $payload->id = $response['USERID'];
        $payload->email = $response['email'];
    
        http_response_code(200);
    
        $response['password'] = ''; //For Security
        echo json_encode(
            array( 
                "code" => 200,
                "message" => "Thành công!",
                "user" => $response,
                "token" => JWT::encode($payload, SERET_SERVER_KEY)
            )
        );
        return;
    }
} else {
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 500,
            "error" => "Dữ liệu nhập không hợp lệ!",
        )
    );
}


http_response_code(200);
echo json_encode(
    array( 
        "status" => 500,
        "error" => "Access denied!",
    )
);