<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, PUT, DELETE, OPTIONS");
header("Access-Control-Max-Age: 86400");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/member.php';

$database = new Database();

$member = new Member($database->conn);

$method = $_SERVER['REQUEST_METHOD'];

$data = $_GET;

if($method == 'GET') {
    $token = substr(getallheaders()['Authorization'], 7);
    
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);
        $result = $member->getUserById($dataDecode->id);
        if(!$result || $result['permission'] != 'admin') {
            http_response_code(401);
            echo json_encode(
                array( 
                    "status" => 401,
                    "error" => "Permission denied!",
                    "message" => getStatusCodeMessage(401)
                )
            );
            return;
        }

    } catch( \Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 401,
                "error" => 'Unauthorized',
                "token" => $token
            )
        );
        return;
    }

    if(isset($data['pagesize']) && isset($data['currPage'])) {
        $result = $member->getUsersPaging( $data['pagesize'], $data['currPage'] );
        http_response_code(200);
        echo json_encode(
            array( 
                "status" => 200,
                "body" => $result
            )
        );
    } else {
        http_response_code(400);
        echo json_encode(
            array( 
                "status" => 400,
                "error" => "Du lieu khong hop le!!",
                "message" => getStatusCodeMessage(400)
            )
        );
    }
} else {
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 500,
            "error" => "Access denied!",
        )
    );
}
