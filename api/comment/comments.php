<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
// include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/comment.php';

$database = new Database();
$comment = new Comment($database->conn);


$method = $_SERVER['REQUEST_METHOD'];
$data = $_GET;

if($method == 'GET' && !empty($_GET['postid'])) {
    $result = $comment->getCommentByPostID($_GET['postid']);
    
    http_response_code(200);
    echo json_encode(
        array( 
            "status" => 200,
            "message" => "Thành công!",
            "comments" => $result
        )
    );
    return;
}

http_response_code(200);
echo json_encode(
    array( 
        "status" => 500,
        "error" => "Access denied!"
    )
);


?>