<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST, PUT, DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, accept, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once '../../api/config.php';
require_once '../../defined.php';
include_once '../../helpers/jwt-helpers.php';

include_once '../../model/database.php';
include_once '../../model/comment.php';
include_once '../../model/member.php';

$database = new Database();
$member = new Member($database->conn);
$comment = new Comment($database->conn);
$method = $_SERVER['REQUEST_METHOD'];
$data = json_decode(file_get_contents("php://input"));

if($method == "POST") {
    $token = substr(getallheaders()['Authorization'], 7);
    try {
        $dataDecode = JWT::decode($token, SERET_SERVER_KEY);
        $result = $member->getUserById($dataDecode->id);
        if(!$result) {
            http_response_code(400);
            echo json_encode(
                array( 
                    "status" => 400,
                    "error" => "Token invalid!",
                    "system" => error_get_last()
                )
            );
            return;
        }
    } catch (\Throwable $th) {
        http_response_code(401);
        echo json_encode(
            array( 
                "status" => 401,
                "error" => 'Unauthorized',
                "token" => $token,
                "system" => error_get_last()
            )
        );
        return;
    }

    if( !empty($data->comment) &&
        !empty($data->postid)) {
        $result = $comment->createNewComment($data, $dataDecode->id);
        if($result) {
            http_response_code(200);
            echo json_encode(
                array( 
                    "status" => 200,
                    "message" => "Bình luận thành công!",
                    "body" => $result
                )
            );
        } else {
            http_response_code(500);
            echo json_encode(
                array( 
                    "status" => 500,
                    "error" => "Có lỗi xảy ra khi thao tác với cơ sở dữ liệu!",
                    "message" => getStatusCodeMessage(500),
                    "system" => error_get_last()
                )
            );
        }
    } else {
        http_response_code(200);
        echo json_encode(
            array( 
                "status" => 400,
                "error" => "Có lỗi xảy ra, vui lòng thử lại!",
                "message" => getStatusCodeMessage(400)
            )
        );
    }  
    return;
}

http_response_code(200);
echo json_encode(
    array( 
        "status" => 500,
        "error" => "Access denied!"
    )
);