<?php 
class JWT {
    public static function encode($payload, $secret_key) {
        $header = json_encode(
            [
                'typ' => 'JWT', 
                'alg' => 'HS256'
            ]
        );
        $payload = json_encode($payload);
        $base64UrlHeader = base64_encode($header);
        $base64UrlPayload = base64_encode($payload);
        
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret_key, true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        return $jwt;
    }
    public static function decode($token, $secret_key) {
        $tokenSplit = explode('.', $token);
		if (count($tokenSplit) != 3) {
            throw new Exception('Wrong number of segments');
        }
        list($headb64, $bodyb64, $cryptob64) = $tokenSplit;        
        
        $header = json_decode(base64_decode($headb64));
        if($header === null) {
            throw new Exception('Invalid segment encoding');
        }
        
        $payload = json_decode(base64_decode($bodyb64));
        if($payload === null) {
            throw new Exception('Invalid segment encoding');
        }

        $sigExpected = hash_hmac('sha256', $headb64 . "." . $bodyb64, $secret_key, true);
        $sigb64 = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($sigExpected));

        if($sigb64 != $cryptob64) {
            throw new Exception('Signature verification failed');
        }
        return $payload;
    }
}
?>