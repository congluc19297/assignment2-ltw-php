# Assignment 2 - Lập trình Web

# 1. User

## 1.1 POST - register
- permission: none
- url: http://localhost/assignment2-ltw-php/api/member/register.php
- data: (JSON)
    ```
    {
        email: string,
        fullname: string,
        password: string,
        repassword: string,
    }
    ```
- response:
1. 200:
    ```
    {
        code: int,
        message: string,
        user: Object,
        token: string
    }
    ```
2. 500:
    ```
    {
        status: int,
        error: string
    }
    ```
## 1.2 POST - login
- permission: none
- url: http://localhost/assignment2-ltw-php/api/member/login.php
- data: (JSON)
    ```
    {
        email: string,
        password: string
    }
    ```
- response:
1. 200:
    ```
    {
        code: int,
        message: string,
        user: Object,
        token: string
    }
    ```
2. 500:
    ```
    {
        status: int,
        error: string
    }
    ```
## 1.2 GET - member
- permission: Authorization: Bearer `token`
- url: http://localhost/assignment2-ltw-php/api/member/member.php
- data: (JSON)
    ```
    {
        userid: int
    }
    ```
- response:
1. 200:
    ```
    {
        status: int,
        message: string,
        user: Object
    }
    ```
2. 500:
    ```
    {
        status: int,
        error: string
    }
    ```
3. 401:
    ```
    {
        status: int,
        error: string,
        token: string
    }
    ```
## 1.3 POST - update member
- permission: Authorization: Bearer `token`
- url: http://localhost/assignment2-ltw-php/api/member/update.php
- data: (Form Data)
    ```
    {
        avatar: Object File (optional),
        fullname: string,
        description: string,
        gender: string
    }
    ```

## 1.4 POST - change password
- permission: Authorization: Bearer `token`
- url: http://localhost/assignment2-ltw-php/api/member/password.php
- data:
    ```
    {
        oldPassword: string,
        newPassword: string,
        reNewPassword: string
    }
    ```
- response: `{ status, error, message }`
1. 400
    ```
    {
        "error" => "Mật khẩu xác nhận không khớp hoặc không được trùng với mật khẩu cũ"
    }
    ```
2. 400
    ```
    {
        "error" => "Vui lòng nhập đầy đủ thông tin"
    }
    ```
3. 401
    ```
    {
        "error" => "Mật khẩu cũ không đúng. Xin mời nhập lại"
    }
    ```
4. 401
    ```
    {
        "error" => "Token không hợp lệ."
    }
    ```
5. 405
    ```
    {
        "error" => "Có lỗi xảy ra. Vui lòng thử lại"
    }
    ```
6. 500
    ```
    {
        "error" => "Có lỗi xảy ra khi thao tác với cơ sở dữ liệu. Vui lòng thử lại!"
    }
    ```
7. 200


# 2. Post (News)

## 2.1 POST - Create New Post
- permission: Authorization: Bearer `token`
- url: http://localhost/assignment2-ltw-php/api/post/addNew.php
- data: Form Data
    ```
    {
        obj_image: object binary file (Optional),
        url_image: string,
        post_content: string,
        category: string,
    }
    ```

## 2.2 GET - Get Signle Post By Post ID
- permission: Authorization: Bearer `token`
- url: http://localhost/assignment2-ltw-php/api/post/post.php
- data: JSON
    ```
    {
        postid: String
    }
    ```
