<?php
    define ('DS'				, '/');
    define ('ROOT_PROJECT'      ,'assignment2-ltw-php');
    define ('ROOT_PATH'			, dirname(__FILE__));
    

    //localhost:80
    define ('ROOT_ORIGIN'		, 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']);
    
    define ('API_PATH'		    , ROOT_PATH . DS . 'api' . DS);
    define ('PUBLIC_PATH'		, ROOT_PATH . DS . 'public' . DS);
    define ('PUBLIC_PATH_USER'	, PUBLIC_PATH . 'user' . DS);
    define ('PUBLIC_PATH_POST'	, PUBLIC_PATH . 'post' . DS);
    
    define ('ORIGIN_PUBLIC_PATH', ROOT_ORIGIN . DS . ROOT_PROJECT . DS . 'public' . DS);
    define ('ORIGIN_PUBLIC_PATH_USER', ROOT_ORIGIN . DS . ROOT_PROJECT . DS . 'public' . DS . 'user' . DS);
    define ('ORIGIN_PUBLIC_PATH_POST', ROOT_ORIGIN . DS . ROOT_PROJECT . DS . 'public' . DS . 'post' . DS);

    function getStatusCodeMessage($status){
        $codes = Array(
            100 => "Continue",
            101 => "Switching Protocols",
            200 => "OK",
            201 => "Created",
            202 => "Accepted",
            203 => "Non-Authoritative Information",
            204 => "No Content",
            205 => "Reset Content",
            206 => "Partial Content",
            300 => "Multiple Choices",
            301 => "Moved Permanently",
            302 => "Found",
            303 => "See Other",
            304 => "Not Modified",
            305 => "Use Proxy",
            306 => "(Unused)",
            307 => "Temporary Redirect",
            400 => "Bad Request",
            401 => "Unauthorized",
            402 => "Payment Required",
            403 => "Forbidden",
            404 => "Not Found",
            405 => "Method Not Allowed",
            406 => "Not Acceptable",
            407 => "Proxy Authentication Required",
            408 => "Request Timeout",
            409 => "Conflict",
            410 => "Gone",
            411 => "Length Required",
            412 => "Precondition Failed",
            413 => "Request Entity Too Large",
            414 => "Request-URI Too Long",
            415 => "Unsupported Media Type",
            416 => "Requested Range Not Satisfiable",
            417 => "Expectation Failed",
            500 => "Internal Server Error",
            501 => "Not Implemented",
            502 => "Bad Gateway",
            503 => "Service Unavailable",
            504 => "Gateway Timeout",
            505 => "HTTP Version Not Supported"
        );
        
        return (isset($codes[$status])) ? $codes[$status] : '';
    }