-- getListPostByUserID has count comment 
SELECT `UserPost`.*, `NumberComment`.`count`
FROM (
    SELECT `members`.`USERID`,`members`.`fullname`, `members`.`profilepicture`, `posts`.`url_image`, `posts`.`PID`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status` 
    FROM `posts` 
    INNER JOIN `members` 
    ON `members`.`USERID` = 3 AND `members`.`USERID` = `posts`.`USERID` 
    ORDER BY `posts`.`time_added` DESC
) AS `UserPost`
LEFT JOIN (
    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` 
    FROM `comments`
    GROUP BY `comments`.`PID`
) AS `NumberComment`
ON `UserPost`.`PID`=`NumberComment`.`PID`



-- Search Post by Post Content or Fullname
SELECT * 
FROM (
    SELECT `members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`
    FROM `posts`
    LEFT JOIN `members`
    ON `members`.`USERID` = `posts`.`USERID`
) AS `search_table`
WHERE `search_table`.`post_content` like '%Trần Công Lực%' OR `search_table`.`fullname` like '%Trần Công Lực%'
ORDER BY `search_table`.`time_added` DESC

-- Search List User has count comment
SELECT `UserPost`.*, `NumberComment`.`count`
FROM (
    SELECT * 
    FROM (
        SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`
        FROM `posts`
        LEFT JOIN `members`
        ON `members`.`USERID` = `posts`.`USERID`
    ) AS `search_table`
    WHERE `search_table`.`post_content` like '%truc%' OR `search_table`.`fullname` like '%truc%'
) AS `UserPost`
LEFT JOIN (
    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` 
    FROM `comments`
    GROUP BY `comments`.`PID`
) AS `NumberComment`
ON `UserPost`.`PID`=`NumberComment`.`PID`


-- Select paging
SELECT `UserPost`.*, `NumberComment`.`count`
FROM (
    SELECT * 
    FROM (
        SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`
        FROM `posts`
        LEFT JOIN `members`
        ON `members`.`USERID` = `posts`.`USERID`
        ORDER BY `posts`.`time_added` DESC
        LIMIT 0,5
    ) AS `search_table`
) AS `UserPost`
LEFT JOIN (
    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` 
    FROM `comments`
    GROUP BY `comments`.`PID`
) AS `NumberComment`
ON `UserPost`.`PID`=`NumberComment`.`PID`

SELECT * 
FROM (
    SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `posts`.`PID`, `posts`.`url_image`, `posts`.`post_content`, `posts`.`time_added`, `posts`.`status`
    FROM `posts`
    LEFT JOIN `members`
    ON `members`.`USERID` = `posts`.`USERID`
    ORDER BY `posts`.`time_added` DESC
    LIMIT 0,5
) AS `search_table`




-- Get Post By Tag_Index
SELECT `posts`.*, `POST_TAG_PID`.`tag_index`
FROM (
	SELECT `post_tags`.`PID`, `post_tags`.`tag_index`
	FROM `post_tags` WHERE `post_tags`.`tag_index`=1
) `POST_TAG_PID`
LEFT JOIN `posts`
ON `posts`.`PID`=`POST_TAG_PID`.`PID`




SELECT * 
FROM (
    SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `_posts`.`PID`, `_posts`.`url_image`, `_posts`.`post_content`, `_posts`.`time_added`, `_posts`.`status`
    FROM (
        SELECT `posts`.*, `POST_TAG_PID`.`tag_index`
        FROM (
            SELECT `post_tags`.`PID`, `post_tags`.`tag_index`
            FROM `post_tags` WHERE `post_tags`.`tag_index`=1
        ) `POST_TAG_PID`
        LEFT JOIN `posts`
        ON `posts`.`PID`=`POST_TAG_PID`.`PID`
        ORDER BY `posts`.`time_added` DESC
        LIMIT 0,5
    ) AS `_posts`
    LEFT JOIN `members`
    ON `members`.`USERID` = `_posts`.`USERID`
) AS `search_table`


-- Loc post = categories co paging
SELECT `UserPost`.*, `NumberComment`.`count`
FROM (
    SELECT * 
    FROM (
        SELECT `members`.`USERID`,`members`.`profilepicture`, `members`.`fullname`, `_posts`.`PID`, `_posts`.`url_image`, `_posts`.`post_content`, `_posts`.`time_added`, `_posts`.`status`, `_posts`.`tag_index`, `_posts`.`tag_value`
        FROM (
            SELECT `posts`.*, `POST_TAG_PID`.`tag_index`, `POST_TAG_PID`.`tag_value`
            FROM (
                SELECT `post_tags`.`PID`, `post_tags`.`tag_index`, `post_tags`.`tag_value`
                FROM `post_tags` WHERE `post_tags`.`tag_index`=1
            ) `POST_TAG_PID`
            LEFT JOIN `posts`
            ON `posts`.`PID`=`POST_TAG_PID`.`PID`
            ORDER BY `posts`.`time_added` DESC
            LIMIT 0,5
        ) AS `_posts`
        LEFT JOIN `members`
        ON `members`.`USERID` = `_posts`.`USERID`
    ) AS `search_table`
) AS `UserPost`
LEFT JOIN (
    SELECT COUNT(`comments`.`PID`) AS `count`, `comments`.`PID` 
    FROM `comments`
    GROUP BY `comments`.`PID`
) AS `NumberComment`
ON `UserPost`.`PID`=`NumberComment`.`PID`