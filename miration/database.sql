CREATE DATABASE `assignment2`;
USE `assignment2`;

DROP TABLE IF EXISTS members;
CREATE TABLE `members` (
  `USERID` bigint(20) NOT NULL auto_increment,
  `email` varchar(80) NOT NULL default '',
  `username` varchar(80) NOT NULL default '',
  `password` text NOT NULL default '',
  `fullname` varchar(200) NOT NULL default '',
  `gender` varchar(6) NOT NULL default '',
  `description` text NOT NULL,
  `yourviewed` int(20) NOT NULL default '0',
  `profileviews` int(20) NOT NULL default '0',
  `youviewed` bigint(20) NOT NULL default '0',
  `lastlogin` varchar(20) NOT NULL default '',
  `status` enum('1','0') NOT NULL default '1',
  `profilepicture` text NOT NULL default '',
  `permission` enum('_admin', '_member') NOT NULL default '_member',
  PRIMARY KEY  (`USERID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS posts;
CREATE TABLE `posts` (
    `PID` bigint(20) NOT NULL auto_increment,
    `USERID` bigint(20) NOT NULL default '0',
    `url_image` TEXT NOT NULL default '',
    `post_content` LONGTEXT NOT NULL default '',
    `status` enum('1','0') NOT NULL default '1',
    `time_added` int(20) NOT NULL,
    PRIMARY KEY  (`PID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS post_tags;
CREATE TABLE `post_tags` (
    `TAG_ID` bigint(20) NOT NULL auto_increment,
    `PID` bigint(20) NOT NULL default '0',
    `tag_index` int(20) NOT NULL default '0',
    `tag_value` varchar(36) NOT NULL default '',
    PRIMARY KEY  (`TAG_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS posts_favorited;
CREATE TABLE `posts_favorited` (
  `FID` bigint(20) NOT NULL auto_increment,
  `USERID` bigint(25) NOT NULL default '0',
  `PID` bigint(25) NOT NULL default '0',
  PRIMARY KEY  (`FID`),
  UNIQUE KEY `USERID` (`USERID`,`PID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS posts_unfavorited;
CREATE TABLE `posts_unfavorited` (
  `FID` bigint(20) NOT NULL auto_increment,
  `USERID` bigint(25) NOT NULL default '0',
  `PID` bigint(25) NOT NULL default '0',
  PRIMARY KEY  (`FID`),
  UNIQUE KEY `USERID` (`USERID`,`PID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS comments;
CREATE TABLE `comments` (
  `CID` bigint(20) NOT NULL auto_increment,
  `USERID` bigint(25) NOT NULL default '0',
  `PID` bigint(25) NOT NULL default '0',
  `comment` varchar(200) NOT NULL default '',
  `time_added` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY  (`CID`),
  FOREIGN KEY (`USERID`) REFERENCES `members` (`USERID`),
  FOREIGN KEY (`PID`) REFERENCES `posts` (`PID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE `posts` CHANGE `time_added` `time_added` DATETIME(6) NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `post_tags` ADD UNIQUE( `PID`, `tag_index`);
ALTER TABLE `posts` ADD FOREIGN KEY(`USERID`) REFERENCES `members`(`USERID`);
ALTER TABLE `post_tags` ADD FOREIGN KEY`FOREIGN`(`PID`) REFERENCES `posts`(`PID`);
-- ALTER TABLE `posts` CHANGE `time_added` `time_added` INT(20) NOT NULL;

-- INSERT INTO `post_tags` VALUES ('Ảnh troll');
-- INSERT INTO `post_tags` VALUES ('Cách gặp ma');
-- INSERT INTO `post_tags` VALUES ('Clip funvl');
-- INSERT INTO `post_tags` VALUES ('Xác ướp');
-- INSERT INTO `post_tags` VALUES ('Âo tưởng sức mạnh');
-- INSERT INTO `post_tags` VALUES ('Running man');
-- INSERT INTO `post_tags` VALUES ('Ânh bựa vl');
-- INSERT INTO `post_tags` VALUES ('Faptv');
-- INSERT INTO `post_tags` VALUES ('500 anh em');
-- INSERT INTO `post_tags` VALUES ('Ancient aliens');
-- INSERT INTO `post_tags` VALUES ('Video cảm động');
-- INSERT INTO `post_tags` VALUES ('Siêu nhân');
-- INSERT INTO `post_tags` VALUES ('Video kinh dị');
-- INSERT INTO `post_tags` VALUES ('Thánh cuồng');
-- INSERT INTO `post_tags` VALUES ('Comment hài');
-- INSERT INTO `post_tags` VALUES ('Nhạc Remix hay');
-- INSERT INTO `post_tags` VALUES ('Ảnh ấn tượng');
-- INSERT INTO `post_tags` VALUES ('Pháo hoa Tết');
-- INSERT INTO `post_tags` VALUES ('Ji Suk Jin');
-- INSERT INTO `post_tags` VALUES ('Cao Bá Hưng');
-- INSERT INTO `post_tags` VALUES ('Hài Quang Thắng');
-- INSERT INTO `post_tags` VALUES ('Xăm trổ');
-- INSERT INTO `post_tags` VALUES ('Chuyện tình lãng mạn');
-- INSERT INTO `post_tags` VALUES ('Giang hồ');
-- UPDATE `members` SET `gender` = 'nam' WHERE `members`.`USERID` = 2;
